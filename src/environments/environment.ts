// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDqx_HGJm0w7Yz-2QrCpjkCqHTooyuElFQ",
    authDomain: "ehubproj.firebaseapp.com",
    databaseURL: "https://ehubproj.firebaseio.com",
    projectId: "ehubproj",
    storageBucket: "ehubproj.appspot.com",
    messagingSenderId: "188914320681",
    appId: "1:188914320681:web:bb59caeace221f079ae338",
    measurementId: "G-WTQ5J7WRBT"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
