import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'create-idea',
    loadChildren: () => import('./idea/create-idea/create-idea.module').then( m => m.CreateIdeaPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./user/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./user/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./user/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'verifyemail',
    loadChildren: () => import('./user/verifyemail/verifyemail.module').then( m => m.VerifyemailPageModule)
  },
  {
    path: 'resetaccount',
    loadChildren: () => import('./user/resetaccount/resetaccount.module').then( m => m.ResetaccountPageModule)
  },
  {
    path: 'detailIdea/:id',
    loadChildren: () => import('./idea/detail-idea/detail-idea.module').then( m => m.DetailIdeaPageModule)
  }
  // ,
  // {
  //   path: 'entrepreneurs',
  //   loadChildren: () => import('./entrepreneur/entrepreneurs/entrepreneurs.module').then( m => m.EntrepreneursPageModule)
  // },
  // {
  //   path: 'ideas',
  //   loadChildren: () => import('./idea/ideas/ideas.module').then( m => m.IdeasPageModule)
  // },
  // {
  //   path: 'investors',
  //   loadChildren: () => import('./investor/investors/investors.module').then( m => m.InvestorsPageModule)
  // }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
