import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EntrepreneursPageRoutingModule } from './entrepreneurs-routing.module';

import { EntrepreneursPage } from './entrepreneurs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EntrepreneursPageRoutingModule
  ],
  declarations: [EntrepreneursPage]
})
export class EntrepreneursPageModule {}
