import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { EntrepreneurService, Entrepreneur } from 'src/app/service/entrepreneur.service';
import { AuthenticationService } from 'src/app/service/authentication.service';



@Component({
  selector: 'app-entrepreneurs',
  templateUrl: './entrepreneurs.page.html',
  styleUrls: ['./entrepreneurs.page.scss'],
})

export class EntrepreneursPage implements OnInit{

  private entrepreneurs: Observable<Entrepreneur[]>;
  private isLoggedIn: Boolean = false;

  constructor(
    private entrepreneurService: EntrepreneurService,
    private authenticationService: AuthenticationService
    ) {
    if(!this.entrepreneurs){
      this.entrepreneurs = this.entrepreneurService.getEntrepreneurs();
    }
    this.listenForLoginEvents();
  }

  ngOnInit() {
    // this.listenForLoginEvents();
  }

  listenForLoginEvents(){
    this.authenticationService.getLoginStatus().subscribe((status) => {
      console.log("EntrepreneursPage::login status: ", status);
      if(status == 'user:login')
        this.isLoggedIn = true;
      else if(status == 'user:logout')
        this.isLoggedIn = false;
    });
  }
}
