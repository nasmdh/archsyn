import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EntrepreneursPage } from './entrepreneurs.page';

const routes: Routes = [
  {
    path: '',
    component: EntrepreneursPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EntrepreneursPageRoutingModule {}
