import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InvestorsPageRoutingModule } from './investors-routing.module';

import { InvestorsPage } from './investors.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InvestorsPageRoutingModule
  ],
  declarations: [InvestorsPage]
})
export class InvestorsPageModule {}
