import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InvestorsPage } from './investors.page';

describe('InvestorsPage', () => {
  let component: InvestorsPage;
  let fixture: ComponentFixture<InvestorsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestorsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InvestorsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
