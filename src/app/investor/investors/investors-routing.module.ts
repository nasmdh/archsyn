import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InvestorsPage } from './investors.page';

const routes: Routes = [
  {
    path: '',
    component: InvestorsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InvestorsPageRoutingModule {}
