import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { InvestorService, Investor } from 'src/app/service/investor.service';



@Component({
  selector: 'app-investors',
  templateUrl: './investors.page.html',
  styleUrls: ['./investors.page.scss'],
})

export class InvestorsPage implements OnInit{

  private investors: Observable<Investor[]>;

  constructor(private investorService: InvestorService) {
    if(!this.investors){
      this.investors = this.investorService.getInvestors();
    }
  }

  ngOnInit() {
  }
}
