import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ResetaccountPage } from './resetaccount.page';

describe('ResetaccountPage', () => {
  let component: ResetaccountPage;
  let fixture: ComponentFixture<ResetaccountPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetaccountPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ResetaccountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
