import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-resetaccount',
  templateUrl: './resetaccount.page.html',
  styleUrls: ['./resetaccount.page.scss'],
})
export class ResetaccountPage implements OnInit {
  
  private auth
  constructor(
    public authService: AuthenticationService,
    public router: Router,
  ) { }

  ngOnInit() {
  }

  resetPass(email){
    this.authService.resetPass(email.value)
    .then((res) => {
      this.router.navigate(['']);
    }).catch((error) => {
      window.alert(error.message)
    })
  }
}
