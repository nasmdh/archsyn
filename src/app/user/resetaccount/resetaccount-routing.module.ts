import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResetaccountPage } from './resetaccount.page';

const routes: Routes = [
  {
    path: '',
    component: ResetaccountPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResetaccountPageRoutingModule {}
