import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ResetaccountPageRoutingModule } from './resetaccount-routing.module';

import { ResetaccountPage } from './resetaccount.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResetaccountPageRoutingModule
  ],
  declarations: [ResetaccountPage]
})
export class ResetaccountPageModule {}
