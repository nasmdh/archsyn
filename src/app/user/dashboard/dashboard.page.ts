import { Component, OnInit, AfterViewInit, OnChanges, OnDestroy, ViewChild} from '@angular/core';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { Router } from "@angular/router";

import { EntrepreneurService } from 'src/app/service/entrepreneur.service';
import { InvestorService } from 'src/app/service/investor.service';
import { Observable } from 'rxjs';
import { IonItemSliding } from '@ionic/angular';
import { Idea } from 'src/app/service/idea.service';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})

export class DashboardPage {

  private role_entrepreneur: Boolean = false;
  private role_investor: Boolean = false;
  private ideas: Observable<Idea[]>;
  
  // @ViewChild(IonItemSliding) ionItemSliding: IonItemSliding;

  constructor(
    public authService: AuthenticationService,
    public router: Router,
    public entrepreneurService: EntrepreneurService,
    private investorService: InvestorService,

  ) { }

  ionViewWillEnter() {
    console.log("ionViewWillEnter")
    this.getIdeas()
  }
  
  /**
   * Get ideas
   */
  getIdeas(){
    let ideas = this.entrepreneurService.getIdeas();
      if (ideas)
        this.ideas = ideas;
  }
  /**
   * Delete idea
   */
  async deleteIdea(ionItemSliding: IonItemSliding, index: number){
    ionItemSliding.close();
    await this.entrepreneurService.deleteIdea(index);
     this.getIdeas();
  }
  /**
   * Register user on different roles based on choice
   */
  registerRoles(){
    if(this.role_entrepreneur)
      this.registerAsEntrepreneur();
    if(this.role_investor)
      this.registerAsInvestor();
    this.router.navigate(['']);
  }
  /**
   * Register as entrepreneur
   */
  registerAsEntrepreneur(){
      this.entrepreneurService.registerAsEntrepreneur()
      .then((res) => {
        console.log('User registered for entrepreneurship!')
        this.authService.SendVerificationMail()
       
      }).catch((error) => {
        window.alert(error.message)
      })
      
  }
  /**
   * Register as investor
   */
  registerAsInvestor(){
    this.investorService.registerAsEntrepreneur()
    .then((res) => {
      console.log('User registered for investorship!')
      this.authService.SendVerificationMail()
    }).catch((error) => {
      window.alert(error.message)
    })
    
  }
  /**
   * Logout
   */

   logout(){
     this.authService.SignOut().then(()=> 
      this.authService.publishLoginStatus("user:logout"));
   }
}