import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthenticationService } from 'src/app/service/authentication.service';
import { EntrepreneurService } from 'src/app/service/entrepreneur.service';
import { InvestorService } from 'src/app/service/investor.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})

export class RegisterPage implements OnInit {


  registerGroup: FormGroup;
  

  constructor(
    public authService: AuthenticationService,
    public entrepreneurService: EntrepreneurService,
    public investorService: InvestorService,
    public router: Router,
    private formBuilder: FormBuilder
  ) { 
    this.registerGroup = this.formBuilder.group({
      displayName: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl(''),
      entrepreneurRole: new FormControl(false),
      investorRole: new FormControl(false)
    });
  }

  ngOnInit(){}

  register(value){
    this.entrepreneurService.registerUser(value);
  }

/**
 * 
 * @param email 
 * @param password 
 */
registerAsEntrepreneur(){
    this.entrepreneurService.registerAsEntrepreneur()
    .then((res) => {
      // Do something here
      this.authService.SendVerificationMail()
    }).catch((error) => {
      window.alert(error.message)
    })
    
}
/**
 * 
 */
registerAsInvestor(){
  this.investorService.registerAsEntrepreneur()
  .then((res) => {
    // Do something here
    this.authService.SendVerificationMail()
  }).catch((error) => {
    window.alert(error.message)
  })
  
}


}