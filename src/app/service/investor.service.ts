import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference} from '@angular/fire/firestore';
import { Observable} from 'rxjs';
import { map} from 'rxjs/operators';

import { AuthenticationService } from './authentication.service';
import { EHUser } from './ehuser.service';
import { Idea } from './idea.service';

export interface Investor
{
  user: EHUser
  ideas?: Idea[];
}

@Injectable({
  providedIn: 'root'
})
export class InvestorService {

  private investors: Observable<Investor[]>;
  private investorCollection: AngularFirestoreCollection<Investor>;

  constructor(
    private investorfs: AngularFirestore,
    private auth: AuthenticationService

  ) { 
    this.investorCollection = this.investorfs.collection<Investor>('investors');
    this.investorCollection.get().subscribe((investorSnapshot) => {
      investorSnapshot.docs.forEach(doc => {
        console.log(doc.data());
      })
    });
    this.investors = this.investorCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getInvestors(): Observable<Investor[]> {
    return this.investors;
  }

  deleteInvestor(id: string): Promise<void> {
    return this.investorCollection.doc(id).delete();
  }
  
  registerAsEntrepreneur(): Promise<DocumentReference> {
    let investor: Investor = {
      user : {uid: this.auth.getUser().uid}
    }
    return this.investorfs.collection('investors').add(investor);
  }
  addIdea(idea: Idea): void {
    var id = this.auth.getUser().uid;

    console.log(id);
    this.investorCollection.doc(id).update(
      {
        ideas:  { title: idea.title, description: idea.description}
      }
    );
  }
}
