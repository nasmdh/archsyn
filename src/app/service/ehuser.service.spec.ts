import { TestBed } from '@angular/core/testing';

import { EhuserService } from './ehuser.service';

describe('EhuserService', () => {
  let service: EhuserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EhuserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
