import { Injectable, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference} from '@angular/fire/firestore';
import { Observable} from 'rxjs';
import { map, take} from 'rxjs/operators';
import { EHUser } from './ehuser.service';

export interface Idea
{
  id?: string,
  title: string,
  shortDescription: string,
  description: string,
  user?: EHUser,
  status: boolean
  // [Symbol.iterator]: any
}
export interface IdeaItem
{
  idea: Idea,
  user: EHUser
}

@Injectable({
  providedIn: 'root'
})
export class IdeaService {

  private ideas: Observable<Idea[]>;
  private ideaCollection: AngularFirestoreCollection<Idea>;

  constructor(
    private ideafs: AngularFirestore
  ) { 
    this.ideaCollection = this.ideafs.collection<Idea>('ideas');
    this.ideaCollection.get().subscribe((stationSnapshot) => {
      stationSnapshot.docs.forEach(doc => {
        console.log(doc.data());
      })
    });
    this.ideas = this.ideaCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
  getIdeas(): Observable<Idea[]> {
    return this.ideas;
  }
  sortIdeasBy(criteria: string): Observable<Idea[]> {
    
    let filteredIdeas: Observable<Idea[]>;
    return this.ideas;
  }
  getIdea(id: string): Observable<Idea> {
    return this.ideaCollection.doc<Idea>(id).valueChanges().pipe(
      take(1),
      map(station => {
        station.id = id;
        return station
      })
    );
  }

  addIdea(idea: Idea): Promise<DocumentReference> {
    return this.ideaCollection.add(idea);
  }
  updateIdea(idea: Idea): Promise<void> {
    return this.ideaCollection.doc(idea.id).update(
      { 
        name: idea.title, 
        description: idea.description
      })
  }
  deleteIdea(id: string): Promise<void> {
    return this.ideaCollection.doc(id).delete();
  }
}
