import { Injectable } from '@angular/core';

export interface Name {
  first: string,
  middle: string,
  last: string
}

export interface Address {
  street: string,
  city: string,
  province: string,
  country: string
}

@Injectable({
  providedIn: 'root'
})
export class TaxonomyService {

  constructor() { }
}
