import { Injectable, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference} from '@angular/fire/firestore';
import { Observable} from 'rxjs';
import { map, take} from 'rxjs/operators';

import { AuthenticationService } from './authentication.service';
import { EHUser} from './ehuser.service';
import { Idea } from './idea.service';
import { Router } from '@angular/router';

export interface Entrepreneur
{
  user: EHUser,
  ideas?: Idea[];
}

@Injectable({
  providedIn: 'root'
})
export class EntrepreneurService{

  private entrepreneurs: Observable<Entrepreneur[]>;
  private entrepreneurCollection: AngularFirestoreCollection<Entrepreneur>;

  constructor(
    private entrepeneurfs: AngularFirestore,
    private auth: AuthenticationService,
    private router: Router
  ) { 
    this.entrepreneurCollection = this.entrepeneurfs.collection<Entrepreneur>('entrepreneurs');
    // this.entrepreneurCollection.get().subscribe((stationSnapshot) => {
    //   stationSnapshot.docs.forEach(doc => {
    //     console.log(doc.data());
    //   })
    // }); 
    this.entrepreneurs = this.entrepreneurCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
  /**
   * Get the list of entrepreneurs
   */
  getEntrepreneurs(): Observable<Entrepreneur[]> {
    return this.entrepreneurs;
  }
  /**
   * Get the list of entrepreneurs
   */
  getEntrepreneur(): Observable<Entrepreneur> {
    var id = this.auth.getUser().uid;

    return this.entrepreneurCollection.doc<Entrepreneur>(id).valueChanges().pipe(
      take(1),
      map(data => {
        // data.id = id;
        return data
      })
    );
  }

  /**
   * register user as an entrepreneur
   */
  registerAsEntrepreneur(){
    let entrepreneur: Entrepreneur = {
      user : { 
        uid: this.auth.getUser().uid,
        displayName: this.auth.getUser().displayName
      }
    }
    return this.entrepeneurfs.collection('entrepreneurs').doc(this.auth.getUser().uid).set(entrepreneur);
  }
  /**
   * Add a new idea for the current entrepreneur
   * @param idea the idea to be added
   */
  addIdea(idea: Idea) {
    var id = this.auth.getUser().uid;

    return this.entrepreneurCollection.doc(id).update({
      ideas:    this.auth.getFirebase().firestore.FieldValue.arrayUnion(idea)
    });
  }
  /**
   * Delete idea of the current
   * @param index 
   */
  deleteIdea(index: number){
    var id = this.auth.getUser().uid;
    console.log("deleteIdea called ")

    let entrepreneur = this.getEntrepreneur();
    var ideas: Idea[];

    entrepreneur.subscribe(entrepreneur => {
      console.log('entrepreneur: ', entrepreneur)

      ideas = entrepreneur.ideas;
      console.log("ideas, before slice: ", ideas)
      ideas.splice(index, 1);
      console.log("ideas, after slice: ", ideas)
      this.entrepreneurCollection.doc(id).update({
        ideas:    ideas
        }).then(()=>{
          console.log("deleted, index: ", index);
        });
    }).unsubscribe;

    
  }

  /**
   * Get ideas of the current user
   */
  getIdeas(): Observable<Idea[]> {
    var id = this.auth.getUser().uid;
    return this.entrepreneurCollection.doc<Entrepreneur>(id).valueChanges().pipe(
      take(1),
      map(entrepreneur => {
        if(entrepreneur)
        {
          console.log("entrepreneur: ", entrepreneur);
          return entrepreneur.ideas;  
        } 
        else 
          return null
      })
    );
  }

  /** 
   * Register user
   */

  registerUser(data: any){
    console.log('entrepreneur role: ', data.entrepreneurRole);
    console.log('investor role: ', data.investorRole);
      this.auth.RegisterUser(data.email, data.password).then((res) => {
        // Do something here
        this.auth.SendVerificationMail();
        if(data.entrepreneurRole)
          this.registerAsEntrepreneur()
        this.router.navigate(['verifyemail']);
    }).catch((error) => {
      window.alert(error.message)
    })
     
     
   }
}
