import { Injectable, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference} from '@angular/fire/firestore';
import { Observable} from 'rxjs';
import { map, take} from 'rxjs/operators';
import { Name, Address } from './taxonomy.service';


export interface EHUser
{
  uid: string,
  displayName: string
  name?: Name,
  sex?: string,
  age?: number,
  address?: Address,
  rating?: number,
  comments?: string[]
}

@Injectable({
  providedIn: 'root'
})
export class EhuserService {

  private users: Observable<EHUser[]>;
  private userCollection: AngularFirestoreCollection<EHUser>;

  constructor() {}

  getUsers(): Observable<EHUser[]> {
    return this.users;
  }
  sortUsersBy(criteria: string): Observable<EHUser[]> {
    
    let filteredEHUsers: Observable<EHUser[]>;
    return this.users;
  }
  
  addUser(user: EHUser): Promise<DocumentReference> {
    return this.userCollection.add(user);
  }
  // updateUser(user: EHUser): Promise<void> {
  //   return this.userCollection.doc(user.id).update(
  //     { 
  //       name: user.name, 
  //       idea: user.idea
  //     })
  // }
  deleteUser(id: string): Promise<void> {
    return this.userCollection.doc(id).delete();
  }
  
}
