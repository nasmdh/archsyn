import { Component, OnInit } from '@angular/core';
import { Observable, pipe, of } from 'rxjs';
import { IdeaService, Idea, IdeaItem } from 'src/app/service/idea.service';
import { EntrepreneurService, Entrepreneur } from 'src/app/service/entrepreneur.service';
import { from } from 'rxjs';
import { filter } from 'rxjs/operators';
import { EHUser } from 'src/app/service/ehuser.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { AuthenticationService } from 'src/app/service/authentication.service';

@Component({
  selector: 'app-ideas',
  templateUrl: './ideas.page.html',
  styleUrls: ['./ideas.page.scss'],
})

export class IdeasPage implements OnInit{

  private ideaItems: Observable<IdeaItem[]>;

  constructor(
    private entrepreneurService: EntrepreneurService,
    private router: Router) {

  }

  ngOnInit(){
    if(!this.ideaItems){
      this.getIdeaItems();
    }
  }
  /**
   * Create idea items from "idea" and "user" elements
   * Both elements are extracted from each entrepreneur element
   */
  getIdeaItems(){
    let entrepreneurs: Observable<Entrepreneur[]> = this.entrepreneurService.getEntrepreneurs();
    let items_: Array<{idea:Idea, user:EHUser}> = [];
    // Extract idea and its owner(user)
    // Create an item that holds both elements
    // Push them to array, one by one
    entrepreneurs.subscribe(entrepreneurs => {
      items_ = [];
      entrepreneurs.forEach(entrepreneur => {
        if(entrepreneur.ideas){
        console.log("getIdeaItems::idea: ", entrepreneur.ideas)
        entrepreneur.ideas.forEach(idea => {
          if (idea.status)
            items_.push({idea: idea, user: entrepreneur.user});
          });
        }
      });
    console.log("items_: ", items_);
    this.ideaItems =  of(items_);

    });
    // Create an observer out of the array, easy to use it in the template
  }

  /**
   * Navigate to detail page with parameter "item
   * @param item 
   */
  navDetail(item: IdeaItem){
    let navigationExtras: NavigationExtras = {
      state: {
        ideaItem: item
      }
    };
    this.router.navigate(['/detailIdea/id'], navigationExtras);
  }
}