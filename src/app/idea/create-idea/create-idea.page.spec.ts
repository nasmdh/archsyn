import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateIdeaPage } from './create-idea.page';

describe('CreateIdeaPage', () => {
  let component: CreateIdeaPage;
  let fixture: ComponentFixture<CreateIdeaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateIdeaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateIdeaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
