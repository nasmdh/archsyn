import { Component, OnInit } from '@angular/core';
import { Idea } from 'src/app/service/idea.service';
import { Entrepreneur, EntrepreneurService } from 'src/app/service/entrepreneur.service';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { title } from 'process';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { IdeasPage } from '../ideas/ideas.page';

@Component({
  selector: 'app-create-idea',
  templateUrl: './create-idea.page.html',
  styleUrls: ['./create-idea.page.scss'],
})
export class CreateIdeaPage implements OnInit {

  ideaGroup = new FormGroup({
      title: new FormControl(''),
      shortDescription: new FormControl(''),
      description: new FormControl(''),
      status: new FormControl('false')
    });
  
  entrepreneur: Entrepreneur

  constructor(
    private entrepreneurService: EntrepreneurService,
    private router: Router) {
   }

  ngOnInit() {
  }

  dumpIdeas(){
    let ideas: Idea[] = [
    {
      title: "Mars",
      shortDescription: "Explore the existence of life",
      description: "Mars is the fourth planet from the Sun and the second-smallest planet in the Solar System, being only larger than Mercury. In English, Mars carries the name of the Roman god of war and is often referred to as the 'Red Planet'.",
      status: true
    },
    {
      title: "Jupiter",
      shortDescription: "Is Jupiter big enough for us, to avoid human conflicts? ",
      description: "Jupiter is the fifth planet from the Sun and the largest in the Solar System. It is a gas giant with a mass one-thousandth that of the Sun, but two-and-a-half times that of all the other planets in the Solar System combined.",
      status: false

    },
    {
      title: "Mercury",
      shortDescription: "Should we classify Mercury as a plant?",
      description: "Mercury is the smallest and innermost planet in the Solar System. Its orbit around the Sun takes 87.97 days, the shortest of all the planets in the Solar System. ",
      status: true

    },
    {
      title: "Kepler-69c",
      shortDescription: "Is it habitable?",
      description: "is a confirmed super-Earth extrasolar planet, likely rocky, orbiting the Sun-like star Kepler-69, the outermost of two such planets discovered by NASA's Kepler spacecraft. It is located about 2,430 light-years (746 parsecs) from Earth.",
      status: false

    }
    
    ];
    ideas.forEach(idea => {
      this.addIdea(idea);
    });
    
  }

  deleteIdea(index: number){
    this.entrepreneurService.deleteIdea(index)
  }

  addIdea(idea: Idea){
    this.entrepreneurService.addIdea(idea).then(() => {
        console.log("Idea created: ", idea);
        this.router.navigate(['']);
      }
    ).catch(()=>{
      console.log("Idea could not be created: ", idea);
      }
    );
  }


}
