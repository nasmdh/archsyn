import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailIdeaPage } from './detail-idea.page';

const routes: Routes = [
  {
    path: '',
    component: DetailIdeaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailIdeaPageRoutingModule {}
