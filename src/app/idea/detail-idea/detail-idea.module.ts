import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailIdeaPageRoutingModule } from './detail-idea-routing.module';

import { DetailIdeaPage } from './detail-idea.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailIdeaPageRoutingModule
  ],
  declarations: [DetailIdeaPage]
})
export class DetailIdeaPageModule {}
