import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IdeaItem } from 'src/app/service/idea.service';

@Component({
  selector: 'app-detail-idea',
  templateUrl: './detail-idea.page.html',
  styleUrls: ['./detail-idea.page.scss'],
})
export class DetailIdeaPage implements OnInit {

  private ideaItem: IdeaItem;
  
  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.ideaItem = this.router.getCurrentNavigation().extras.state.ideaItem;
        // console.log("detail item: ", this.ideaItem)
      }
    });
  }

}
