import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailIdeaPage } from './detail-idea.page';

describe('DetailIdeaPage', () => {
  let component: DetailIdeaPage;
  let fixture: ComponentFixture<DetailIdeaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailIdeaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailIdeaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
