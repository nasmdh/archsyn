import { Component, OnInit, DoCheck } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';


@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage{

  private isLoggedIn: Boolean = false;

  constructor(
    private authenticationService: AuthenticationService
  ) {
    this.listenForLoginEvents();
  }
  // ngDoCheck(){
  //   this.isLoggedIn = this.authenticationService.isLoggedIn()
  //   }
  
  listenForLoginEvents(){
    this.authenticationService.getLoginStatus().subscribe((status) => {
      console.log("TabsPage::login status: ", status);
      if(status == 'user:login')
        this.isLoggedIn = true;
      else if(status == 'user:logout')
        this.isLoggedIn = false;
    });
  }

}
