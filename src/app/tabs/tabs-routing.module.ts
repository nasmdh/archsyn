import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { IonicModule } from '@ionic/angular';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        loadChildren: () => import('../idea/ideas/ideas.module').then(m => m.IdeasPageModule)
      },
      {
        path: 'tab2',
        loadChildren: () => import('../entrepreneur/entrepreneurs/entrepreneurs.module').then(m => m.EntrepreneursPageModule)
      },
      {
        path: 'tab3',
        loadChildren: () => import('../investor/investors/investors.module').then(m => m.InvestorsPageModule)
      },
      {
        path: 'tab4',
        loadChildren: () => import('../user/login/login.module').then(m => m.LoginPageModule)
      },
      {
        path: 'tab41',
        loadChildren: () => import('../user/dashboard/dashboard.module').then(m => m.DashboardPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule, IonicModule]
})
export class TabsPageRoutingModule {}
